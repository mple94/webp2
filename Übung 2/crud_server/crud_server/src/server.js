"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var User_1 = require("./User");
var userList = [];
var app = express();
app.use(express.json());
app.listen(8080, function () {
    console.log('Server started at http://localhost:8080');
});
app.post('/user', function (req, res) {
    // Read data from request body
    var firstName = req.body.firstName;
    var lastName = req.body.lastName;
    var newUser = new User_1.User(firstName, lastName);
    userList.push(newUser);
    res.status(201).send({
        user: newUser,
        message: 'User created'
    });
});
app.get('/user/:userId', function (req, res) {
    // Read data from request parameters
    var userId = req.params.userId;
    // Search user in user list
    for (var _i = 0, userList_1 = userList; _i < userList_1.length; _i++) {
        var user = userList_1[_i];
        if (user.id == userId) {
            res.status(200).send({
                user: user,
                message: 'User fetched'
            });
            // Terminate this route
            return;
        }
    }
    // The requested user was not found
    res.status(404).send({
        message: 'User not found'
    });
});
app.put('/user/:userId', function (req, res) {
    var userId = Number(req.params.userId);
    var firstName = req.body.firstName;
    var lastName = req.body.lastName;
    var userToUpdate = userList.find(function (user) { return user.id === userId; });
    if (!userToUpdate) {
        res.status(404).send({ message: 'User not found' });
    }
    else {
        userToUpdate.firstName = firstName;
        userToUpdate.lastName = lastName;
        res.status(200).send({ user: userToUpdate, message: 'User updated' });
    }
});
app.delete('/user/:userId', function (req, res) {
    var userId = Number(req.params.userId);
    var userIndex = userList.findIndex(function (user) { return user.id === userId; });
    if (userIndex === -1) {
        res.status(404).send({ message: 'User not found' });
    }
    else {
        userList.splice(userIndex, 1);
        res.status(204).send({ message: 'User deleted' });
    }
});
app.get('/users', function (req, res) {
    res.status(200).send({
        users: userList,
        message: 'List of users'
    });
});
