/**
 * A user
 */
var User = /** @class */ (function () {
    function User(givenName, familyName, userName) {
        // Shortcut Syntax: 1) Assign userCounter. 2) increment by 1.
        this.id = User.userCounter++;
        this.givenName = givenName.trim();
        this.familyName = familyName.trim();
        this.userName = userName.trim();
        // `new Date()` creates a date object using the current date and time.
        this.creationTime = new Date();
    }
    /**
     * When creating a new user, userCounter
     * will be used to determine the next user id.
     */
    User.userCounter = 1;
    return User;
}());
/**
 * A list of users.
 * This class is a simple database, which implements
 * the CRUD operators for a list of users.
 */
var UserList = /** @class */ (function () {
    /**
     * Creates an empty user list.
     */
    function UserList() {
        this.users = [];
    }
    /**
     * Returns the user list.
     * Using a private property and a "getter"-method
     * prevents reassignment of the user list.
     */
    UserList.prototype.getUsers = function () {
        return this.users;
    };
    /**
     * Adds a user to the user list.
     */
    UserList.prototype.addUser = function (user) {
        this.users.push(user);
    };
    /**
     * Deletes the user who is identified by the given id.
     * Returns `true` if the user could be found and deleted.
     */
    UserList.prototype.deleteUser = function (userId) {
        // Search for a user that has the given id using a loop.
        for (var i = 0; i < this.users.length; i++) {
            // Check if the user's id matches the given id.
            if (this.users[i].id == userId) {
                // Remove the user from the array.
                this.users.splice(i, 1);
                // Return true, because the user was found.
                return true;
            }
        }
        // If the loop finishes without returning, the user could not be found.
        return false;
    };
    /**
     * Returns a single user which has the given user id.
     * Important: Do not confuse the user's id with their index in the user list.
     */
    UserList.prototype.getUser = function (userId) {
        // Search for the user by using a loop.
        for (var _i = 0, _a = this.users; _i < _a.length; _i++) {
            var user = _a[_i];
            // Check if the user's id matches the given user id.
            if (userId === user.id) {
                // The id matches, so return the user.
                return user;
            }
        }
        // The user could not be found. Return `null`.
        return null;
    };
    /**
     * Updates the properties of a user who has the given user id.
     * Returns `true`, if the user was found and updated.
     * Returns `false` if no user was found.
     */
    UserList.prototype.editUser = function (userId, givenName, familyName, userName) {
        // Search for the user using a loop
        for (var _i = 0, _a = this.users; _i < _a.length; _i++) {
            var user = _a[_i];
            // Check if the user's id matches the given user id.
            if (user.id === userId) {
                // User found. Update their properties by assigning the given values.
                user.givenName = givenName;
                user.familyName = familyName;
                user.userName = userName;
                // Return `true` because a user was found and updated.
                return true;
            }
        }
        // If the loop finishes without returning, no user was found. Return `false`.
        return false;
    };
    return UserList;
}());
/**
 * The user list.
 */
var userList = new UserList();
document.addEventListener("DOMContentLoaded", function () {
    // Add 3 demo entries for test purposes.
    userList.addUser(new User("Samuel", "Schepp", "sam123"));
    userList.addUser(new User("Kevin", "Linne", "kev3421"));
    userList.addUser(new User("Peter", "Kneisel", "pett332"));
    renderList();
    updateDeveloperDashboard();
    // Event handler of the add user button
    document.getElementById("add-user-form").addEventListener("submit", function (event) {
        event.preventDefault();
        var givenNameEl = document.getElementById("add-user-given-name");
        var familyNameEl = document.getElementById("add-user-family-name");
        var userNameEl = document.getElementById("add-user-user-name");
        var givenName = givenNameEl.value;
        var familyName = familyNameEl.value;
        var userName = userNameEl.value;
        // Check, if any given value is empty.
        // Don't allow creation of users without given name or family name or username.
        if (givenName.length == 0 || familyName.length == 0 || userName.length == 0) {
            addMessage("The given name or family name or user name is empty.");
            return;
        }
        // Create the new user.
        var user = new User(givenName, familyName, userName);
        // Add the user to the user list.
        userList.addUser(user);
        addMessage("User added.");
        // Update the html
        renderList();
        updateDeveloperDashboard();
        // Clear the input fields
        givenNameEl.value = "";
        familyNameEl.value = "";
        userNameEl.value = "";
    });
    // Handler of the modal's 'save' button
    document.getElementById("edit-user-form").addEventListener("submit", function (event) {
        event.preventDefault();
        var idEl = document.getElementById("edit-user-id");
        var givenNameEl = document.getElementById("edit-user-given-name");
        var familyNameEl = document.getElementById("edit-user-family-name");
        var userNameEl = document.getElementById("edit-user-user-name");
        // Read the user's id from the hidden field.
        var userId = Number(idEl.value);
        // Perform the update
        userList.editUser(userId, givenNameEl.value, familyNameEl.value, userNameEl.value);
        addMessage("User updated.");
        // Hide the modal window
        bootstrap.Modal.getInstance(document.getElementById("edit-user-modal")).hide();
        // Update the html
        updateDeveloperDashboard();
        renderList();
    });
});
/**
 * 1) Clears the user table.
 * 2) Adds all users to the table.
 */
function renderList() {
    var userListEl = document.getElementById("user-list");
    // Remove all entries from the table
    userListEl.replaceChildren();
    var _loop_1 = function (user) {
        // The new table row
        var tr = document.createElement("tr");
        // ID cell
        var tdId = document.createElement("td");
        tdId.textContent = user.id.toString();
        // Given name cell
        var tdGivenName = document.createElement("td");
        tdGivenName.textContent = user.givenName;
        // Family name cell
        var tdFamilyName = document.createElement("td");
        tdFamilyName.textContent = user.familyName;
        // Username cell
        var tdUsername = document.createElement("td");
        tdUsername.textContent = user.userName;
        // Creation date cell
        var tdDate = document.createElement("td");
        tdDate.textContent = user.creationTime.toLocaleString();
        // Buttons cell
        var tdButtons = document.createElement("td");
        // Delete button
        var deleteButton = document.createElement("button");
        deleteButton.className = "btn btn-danger";
        deleteButton.addEventListener("click", function () {
            userList.deleteUser(user.id);
            addMessage("User deleted.");
            renderList();
            updateDeveloperDashboard();
        });
        // Delete button icon
        var deleteButtonIcon = document.createElement("i");
        deleteButtonIcon.className = "fa-solid fa-trash";
        deleteButton.append(deleteButtonIcon);
        // Edit button
        var editButton = document.createElement("button");
        editButton.className = "btn btn-primary ms-3";
        editButton.addEventListener("click", function () {
            showEditModal(user);
        });
        // Edit button icon
        var editButtonIcon = document.createElement("i");
        editButtonIcon.className = "fa-solid fa-pen";
        editButton.append(editButtonIcon);
        // Adds the buttons to the button cell
        tdButtons.append(deleteButton, editButton);
        // Add the cells to the table row
        tr.append(tdId, tdGivenName, tdFamilyName, tdUsername, tdDate, tdButtons);
        // Add the table row to the table
        userListEl.append(tr);
    };
    for (var _i = 0, _a = userList.getUsers(); _i < _a.length; _i++) {
        var user = _a[_i];
        _loop_1(user);
    }
}
/**
 * 1) Fills the modal window with the given user's data.
 * 2) Opens the modal window.
 */
function showEditModal(user) {
    var idEl = document.getElementById("edit-user-id");
    var givenNameEl = document.getElementById("edit-user-given-name");
    var familyNameEl = document.getElementById("edit-user-family-name");
    var userNameEl = document.getElementById("edit-user-user-name");
    // Write the user's id into the hidden field.
    idEl.value = user.id.toString();
    // Write the user's data into the text fields.
    givenNameEl.value = user.givenName;
    familyNameEl.value = user.familyName;
    userNameEl.value = user.userName;
    // Initialise the modal functionality. Enables the methods `.show()` and `.hide()`.
    var modal = new bootstrap.Modal(document.getElementById("edit-user-modal"));
    // Show the modal window.
    modal.show();
}
/**
 * Creates a new alert message.
 */
function addMessage(message) {
    var messagesEl = document.getElementById('messages');
    // The alert element
    var alertEl = document.createElement('div');
    alertEl.classList.add('alert', 'alert-warning', 'alert-dismissible', 'fade', 'show');
    alertEl.setAttribute('role', 'alert');
    alertEl.textContent = message;
    // Close button
    var buttonEl = document.createElement("button");
    // btn-close changes the button into an 'X' icon.
    buttonEl.className = "btn-close";
    // data-bs-dismiss enables the button to automatically close the alert on click.
    buttonEl.setAttribute("data-bs-dismiss", "alert");
    // Add the close button to the alert.
    alertEl.appendChild(buttonEl);
    // Convert to Bootstrap Alert type
    var alert = new bootstrap.Alert(alertEl);
    // Add message to DOM
    messagesEl.appendChild(alertEl);
    // Auto-remove message after 5 seconds (5000ms)
    setTimeout(function () {
        alert.close();
    }, 5000);
}
function removeSpaces(input) {
    var value = input.value;
    value = value.replace(/\s/g, "");
    input.value = value;
}
function updateDeveloperDashboard() {
    var userCount = userList.getUsers().length;
    var nameLengths = userList.getUsers().map(function (user) { return user.givenName.length + user.familyName.length; });
    var totalNameLength = nameLengths.reduce(function (total, length) { return total + length; }, 0);
    var averageNameLength = userCount > 0 ? totalNameLength / userCount : 0;
    var userIds = userList.getUsers().map(function (user) { return user.id; });
    var minUserId = Math.min.apply(Math, userIds);
    var maxUserId = Math.max.apply(Math, userIds);
    document.getElementById("user-count").textContent = userCount.toString();
    document.getElementById("average-name-length").textContent = averageNameLength.toFixed(2);
    document.getElementById("min-user-id").textContent = minUserId.toString();
    document.getElementById("max-user-id").textContent = maxUserId.toString();
}
