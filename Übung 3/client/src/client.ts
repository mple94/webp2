document.addEventListener('DOMContentLoaded', () => {
  const guessForm = document.getElementById("guess-form") as HTMLFormElement | null;
  const resetForm = document.getElementById("reset-form") as HTMLFormElement | null;
  const cheatForm = document.getElementById("cheat-form") as HTMLFormElement | null;
  const resultBox = document.getElementById("result-box") as HTMLDivElement | null;

  if (guessForm) {
      guessForm.addEventListener("submit", async (event) => {
          event.preventDefault();
          const guess = (document.getElementById("guess-input") as HTMLInputElement).value;
          const res = await fetch(`http://localhost:8080/guess/${guess}`);
          const data = await res.json();
          displayResult(data);
      });
  }

  if (resetForm) {
      resetForm.addEventListener("submit", async (event) => {
          event.preventDefault();
          const min = (document.getElementById("reset-min") as HTMLInputElement).value;
          const max = (document.getElementById("reset-max") as HTMLInputElement).value;
          const res = await fetch("http://localhost:8080/reset", {
              method: "post",
              headers: {
                  "Content-Type": "application/json"
              },
              body: JSON.stringify({ min, max }),
          });
          const data = await res.json();
          displayResult(data);
      });
  }

  if (cheatForm) {
      cheatForm.addEventListener("submit", async (event) => {
          event.preventDefault();
          const password = (document.getElementById("cheat-password") as HTMLInputElement).value;
          const res = await fetch("http://localhost:8080/cheat", {
              method: "post",
              headers: {
                  "Content-Type": "application/json"
              },
              body: JSON.stringify({ password }),
          });
          const data = await res.json();
          displayResult(data);
      });
  }

  function displayResult(data: { answer?: string; win?: boolean; message?: string; targetNumber?: number }) {
      const resultElement = document.getElementById("result-box");
      if (resultElement) {
          resultElement.innerText = JSON.stringify(data, null, 2);

          if (resultBox) {
              resultBox.innerText = ""; 

              if (data.answer === 'Your guess was too low!' || data.answer === 'Your guess was too high!') {
                  resultBox.innerText = `Hint: ${data.answer}`;
                  resultBox.className = "alert alert-info"; 
              }else if (data.answer === 'You got it!') {
                  resultBox.innerText = 'Congratulations! You win!';
                  resultBox.className = "alert alert-success"; 
              }else if (data.message === 'Cheat authorized' || data.targetNumber) {
                resultBox.innerText = `Cheat: ${data.targetNumber}`;
                resultBox.className = "alert alert-info"; 
              }else if (data.message === 'Unauthorized access!') {
                  resultBox.innerText = 'Wrong password for cheat!';
                  resultBox.className = "alert alert-danger";
              }else if (data.message === 'Target number reset successfully!') {
                resultBox.innerText = `Reset successful!`;
                resultBox.className = "alert alert-info";
              }
          }   
      }
  }
});
