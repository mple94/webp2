"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const cors = require("cors");
const path = require("path");
const cheatPassword = 'secret';
const app = express();
const port = 8070;
app.use(cors());
const staticPath = path.join(__dirname, 'public');
app.use(express.static(staticPath));
function random(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}
let targetNumber = random(1, 10);
app.use(express.json());
app.get('/guess/:guess', (req, res) => {
    const userGuess = parseInt(req.params.guess);
    if (isNaN(userGuess)) {
        res.json({ answer: 'Please guess a number!', win: false });
    }
    else if (userGuess < targetNumber) {
        res.json({ answer: 'Your guess was too low!', win: false });
    }
    else if (userGuess > targetNumber) {
        res.json({ answer: 'Your guess was too high!', win: false });
    }
    else {
        res.json({ answer: 'You got it!', win: true });
    }
});
app.post('/reset', (req, res) => {
    const { min, max } = req.body;
    targetNumber = random(min, max);
    res.json({ message: 'Target number reset successfully!' });
});
app.post('/cheat', (req, res) => {
    const { password } = req.body;
    console.log('Received password:', password);
    if (password === cheatPassword) {
        res.json({ message: 'Cheat authorized', targetNumber });
    }
    else {
        res.status(403).json({ message: 'Unauthorized access!' });
    }
});
app.listen(port, () => {
    console.log(`Server is running on http://localhost:${port}`);
});
