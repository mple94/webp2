import express = require('express');

import { Request, Response } from 'express';
import { User } from './User';

let userList: User[] = [];

const app = express();

app.use(express.json());

app.listen(8080, () => {
	console.log('Server started at http://localhost:8080');
});


app.post('/user', (req: Request, res: Response) => {
	// Read data from request body

	let firstName: string = req.body.firstName;
	let lastName: string = req.body.lastName;

	const newUser = new User(firstName, lastName);
	userList.push(newUser);
	res.status(201).send({
		user: newUser,
		message: 'User created'
	});
});


app.get('/user/:userId', (req: Request, res: Response) => {
	// Read data from request parameters
	let userId: number = req.params.userId;
	// Search user in user list
	for (let user of userList) {
		if (user.id == userId) {
			res.status(200).send({
				user: user,
				message: 'User fetched'
			});
			// Terminate this route
			return;
		}
	}
	// The requested user was not found
	res.status(404).send({
		message: 'User not found'
	});
});


app.put('/user/:userId', (req: Request, res: Response) => {
	const userId: number = Number(req.params.userId);
	const firstName: string = req.body.firstName;
	const lastName: string = req.body.lastName;

	const userToUpdate = userList.find((user) => user.id === userId);

	if (!userToUpdate) {
		res.status(404).send({ message: 'User not found' });
	} else {
		userToUpdate.firstName = firstName;
		userToUpdate.lastName = lastName;

		res.status(200).send({ user: userToUpdate, message: 'User updated' });
	}
});


app.delete('/user/:userId', (req: Request, res: Response) => {
	const userId: number = Number(req.params.userId);

	const userIndex = userList.findIndex((user) => user.id === userId);

	if (userIndex === -1) {
		res.status(404).send({ message: 'User not found' });
	} else {
		userList.splice(userIndex, 1);

		res.status(204).send({ message: 'User deleted' });
	}
});


app.get('/users', (req: Request, res: Response) => {
	res.status(200).send({
		users: userList,
		message: 'List of users'
	});
});
