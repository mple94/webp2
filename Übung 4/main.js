"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mysql = require("mysql2");
var connection = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "pet"
});
function deleteDatabase(connection) {
    connection.query("DELETE FROM pet;", function (err) {
        if (err) {
            console.log(err);
        }
        else {
            console.log("Database is connected");
        }
    });
}
function insertData(connection) {
    var pets = [
        { id: 1, name: "Evi", breed: "Russisch Blau", year_of_birth: "2023" },
        { id: 2, name: "Nuri", breed: "Russisch Blau", year_of_birth: "2023" },
        { id: 3, name: "Kater Mau", breed: "Europäisch Kurzhaar", year_of_birth: "2017" },
        { id: 4, name: "Butch", breed: "Old English Bulldog", year_of_birth: "2019" },
        { id: 5, name: "Goliath", breed: "Mix Breed", year_of_birth: "2011" },
    ];
    pets.forEach(function (pet) {
        connection.query("INSERT INTO pet (id, name, breed, year_of_birth) VALUES (".concat(pet.id, ", \"").concat(pet.name, "\", \"").concat(pet.breed, "\", \"").concat(pet.year_of_birth, "\");"), function (err) {
            if (err) {
                console.log(err);
            }
        });
    });
}
function sortData(connection) {
    connection.query("SELECT id, name FROM pet ORDER BY name", function (err, rows) {
        if (err) {
            console.log(err);
        }
        else {
            console.log('All pets sorted by name:');
            for (var i = 0; i < rows.length; i++) {
                var row = rows[i];
                console.log("ID: ".concat(row.id, ". Name: ").concat(row.name));
            }
        }
    });
}
function findOldestPet(connection) {
    connection.query("SELECT name FROM pet ORDER BY year_of_birth ASC LIMIT 1", function (err, rows) {
        if (err) {
            console.log(err);
        }
        else {
            if (rows.length > 0) {
                var oldestPet = rows[0];
                console.log("Oldest pet: ".concat(oldestPet.name));
            }
            else {
                console.log("No pets found.");
            }
        }
    });
}
deleteDatabase(connection);
insertData(connection);
sortData(connection);
findOldestPet(connection);
connection.end();
