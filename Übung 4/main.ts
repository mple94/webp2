import * as mysql from 'mysql2';

const connection =  mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "pet"
    });

function deleteDatabase(connection) {
    connection.query("DELETE FROM pet;", (err) => {
    if (err) {
        console.log(err);
    } else {
        console.log("Database is connected");
    }
    });
}

function insertData(connection) {
    const pets = [
    { id: 1, name: "Evi", breed: "Russisch Blau", year_of_birth: "2023" },
    { id: 2, name: "Nuri", breed: "Russisch Blau", year_of_birth: "2023" },
    { id: 3, name: "Kater Mau", breed: "Europäisch Kurzhaar", year_of_birth: "2017" },
    { id: 4, name: "Butch", breed: "Old English Bulldog", year_of_birth: "2019" },
    { id: 5, name: "Goliath", breed: "Mix Breed", year_of_birth: "2011" },
    ];

    pets.forEach((pet) => {
    connection.query(
        `INSERT INTO pet (id, name, breed, year_of_birth) VALUES (${pet.id}, "${pet.name}", "${pet.breed}", "${pet.year_of_birth}");`,
        (err) => {
        if (err) {
            console.log(err);
        } 
        }
    );
    });
}

function sortData(connection: any) {
    connection.query("SELECT id, name FROM pet ORDER BY name", (err, rows) => {
      if (err) {
        console.log(err);
      } else {
        console.log('All pets sorted by name:');
        for (let i = 0; i < rows.length; i++) {
          const row = rows[i];
          console.log(`ID: ${row.id}. Name: ${row.name}`);
        }
      }
    });
  }

  
function findOldestPet(connection: any) {
    connection.query("SELECT name FROM pet ORDER BY year_of_birth ASC LIMIT 1", (err, rows) => {
    if (err) {
        console.log(err);
    } else {
        if (rows.length > 0) {
        const oldestPet = rows[0];
        console.log(`Oldest pet: ${oldestPet.name}`);
        } else {
        console.log("No pets found.");
        }
    }
    });
}
  

deleteDatabase(connection);
insertData(connection);
sortData(connection);
findOldestPet(connection);

connection.end();

