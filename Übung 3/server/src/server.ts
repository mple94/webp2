import express = require('express');
import { Request, Response } from 'express';
import cors = require('cors');
import path = require('path');

const cheatPassword = 'secret';
const app = express();
const port = 8070;
app.use(cors());
const staticPath = path.join(__dirname, 'public');
app.use(express.static(staticPath));

function random(min: number, max: number): number {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

let targetNumber: number = random(1, 10);

app.use(express.json());

/**
 * @api {get} /guess/:guess Guess the number
 * @apiName GuessNumber
 * @apiGroup Game
 *
 * @apiParam {Number} guess User's guess.
 *
 * @apiSuccess {String} answer Result of the guess.
 * @apiSuccess {Boolean} win Whether the user won or not.
 */
app.get('/guess/:guess', (req: Request, res: Response) => {
  const userGuess = parseInt(req.params.guess);

  if (isNaN(userGuess)) {
    res.json({ answer: 'Please guess a number!', win: false });
  } else if (userGuess < targetNumber) {
    res.json({ answer: 'Your guess was too low!', win: false });
  } else if (userGuess > targetNumber) {
    res.json({ answer: 'Your guess was too high!', win: false });
  } else {
    res.json({ answer: 'You got it!', win: true });
  }
});

/**
 * @api {post} /reset Reset the target number
 * @apiName ResetNumber
 * @apiGroup Game
 *
 * @apiHeader {Number} min Minimum value for the target number.
 * @apiHeader {Number} max Maximum value for the target number.
 *
 * @apiSuccess {String} message Success message.
 */
app.post('/reset', (req: Request, res: Response) => {
  const { min, max } = req.body;
  targetNumber = random(min, max);
  res.json({ message: 'Target number reset successfully!' });
});

/**
 * @api {post} /cheat Cheat to reveal the target number
 * @apiName CheatNumber
 * @apiGroup Game
 *
 * @apiHeader {String} password Cheat password.
 *
 * @apiSuccess {String} message Result of the cheat.
 * @apiSuccess {Number} targetNumber The target number.
 */
app.post('/cheat', (req: Request, res: Response) => {
  const { password } = req.body;
  console.log('Received password:', password);

  if (password === cheatPassword) {
    res.json({ message: 'Cheat authorized', targetNumber });
  } else {
    res.status(403).json({ message: 'Unauthorized access!' });
  }
});

app.listen(port, () => {
  console.log(`Server is running on http://localhost:${port}`);
});
